<p align="center">
  <img src="https://siaprime.net/wp-content/uploads/2018/10/primelogo_about1.png?x99246" width="300" height="300" />
</p>

# PrimeNode

A collection of documentation for running SiaPrime hosts on single board computers. 

* [Raspberry Pi Walkthrough](https://gitlab.com/afdy/primenode/blob/master/docs/walkthrough-rpi3.md#raspberry-pi-3)

* [Raspberry Pi Zero Wireless Walkthrough](https://gitlab.com/afdy/primenode/blob/master/docs/walkthrough-rpi-zero.md#raspberry-pi-zero-wireless)


# Resources
TODO: Obtain more key resource links and add them here.
* SiaPrime Statistics | [SiaPrime.siamining.com](https://SiaPrime.siamining.com/stats)
* Sia fork Stats | [keops.cc/pan-sia](https://keops.cc/pan-sia)
* SiaWiki.Tech | [siawiki.tech](https://siawiki.tech/index)


# Notes
## Original Author
This guide has been modified for SiaPrime, thanks to Sam Sepiol for the original version at https://github.com/e-corp-sam-sepiol/spacenode

## Donations
If you found my guide(s) helpful and want to donate...  

SiaPrime Address     
```
SCP: 43f328162e7c0a2d36b9d3eee0ddaa5950acd99823174669f41b1052294fda267c96cd26fd80
```
