#!/bin/bash
# summary of a SiaPrime node
# 
clear
cd ~/go/bin
echo
./spc consensus
echo
./spc host
echo
./spc gateway list
echo
./spc wallet balance
echo
echo "System uptime: " ; uptime -p
echo
